import os

from dotenv import load_dotenv
import mlflow
import uvicorn
import pandas as pd
from fastapi import FastAPI, UploadFile, File, HTTPException


load_dotenv()
app = FastAPI()
mlflow.set_tracking_uri(os.environ["MLFLOW_TRACKING_URL"])


class Model:

    def __init__(self, name, stage):
        self.model = mlflow.pyfunc.load_model(f"models:/{name}/{stage}")

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions


model = Model("test_classifier", "Production")


@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        return model.predict(data).tolist()

    else:
        return HTTPException(status_code=400, detail="Invalid file format. Only csv is accepted")
