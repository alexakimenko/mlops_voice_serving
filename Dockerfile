FROM python:3.9

WORKDIR /code

RUN pip install --upgrade pip

COPY ./requirements.txt /code/
COPY ./main.py /code/
COPY ./.env /code/

RUN pip install -r requirements.txt

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
